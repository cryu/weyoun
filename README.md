# Micro-SCI / Franklin Apple ][ Floppy Drive Controller

This is a clone of the Micro-SCI / Franklin floppy drive controller.

v0.1 is a direct clone, reverse-engineered from a Micro-SCI board.  It was cross-checked with a Franklin-branded board, as well as the (belatedly discovered) official Franklin schematic (included in doc/service_and_schematics.pdf)

v0.2 has the following enhancements over the original design:

* the 2708 EPROM has been replaced with a more-common 2716/2732 EPROM
* decoupling capacitors have been added
* footprints for all capacitors and resistors have been modernized
* firmware EPROM bank selection jumpers replaced with a DIP switch
* obsolete MPS-U51 transistor replaced with modern equivalent
* clearly-marked calibration test points have been added
* ground pours used to increase operational stability
* support for using Floppy Emu in dual-drive emulation mode with one cable

Firmware images can be built via the Makefile in the firmware subdirectory.  The build framework requires cc65.

![usci](usci.png?raw=true "usci")

## EPROM bank select switches (J4)

* 1:off 2:off - firmware "put jumpers here" splash screen (0x0300)
* 1:off 2: on - 13-sector firmware (0x0200)
* 1: on 2:off - 16-sector firmware (0x0100)
* 1: on 2: on - controller test firmware (0x0000)

## Calibration procedure

* set J4 to on/on ("controller test firmware")
* connect oscilloscope to TP1
* start machine
* execute "call 50688"
* execute "AAW" (and immediately hit ESC)
* adjust R1 until waveform period is 5.4 microseconds
* connect oscilloscope to TP2
* execute "99W" (and immediately hit ESX)
* adjust R2 until waveform period is 3.8 microseconds
* card is now calibrated, power off machine and set J4 to "16-sector firmware"

## Construction notes

* J3 can be omitted if this controller is used with a FloppyEmu
* All discrete logic ICs (except U15, the 74LS373) can be CMOS (74ACT, 74HCT).  As is unfortunately usual with the Apple ][, U15 must be LS due to timing issues.
* if CMOS logic is used, then Q1/R6/R7 can be omitted -- the entire logic array can remain powered up at all times.  Connect pads 1 and 3 on the Q1 footprint.
