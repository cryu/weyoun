; Convert characters to screen codes
; Modified to pass through codes below 12

; Helper macro that converts and outputs one character
.macro _screencode char
        .if (char < 256)
            .if (char > 11)
                .byte (char | $80)
            .else
                .byte (char)
            .endif
        .else
                .error  "screencode: Character constant out of range"
        .endif
.endmacro

.macro  screencode arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9

        ; Bail out if next argument is empty
        .if     .blank (arg1)
                .exitmacro
        .endif

        ; Check for a string
        .if     .match ({arg1}, "")

                ; Walk over all string chars
                .repeat .strlen (arg1), i
                        _screencode        {.strat (arg1, i)}
                .endrepeat

        ; Check for a number
        .elseif .match (.left (1, {arg1}), 0)

                ; Just output the number
                _screencode        arg1

        ; Check for a character
        .elseif .match (.left (1, {arg1}), 'a')

                ; Just output the character
                _screencode        arg1

        ; Anything else is an error
        .else

                .error  "screencode: invalid argument type"

        .endif

        ; Call the macro recursively with the remaining args
        screencode arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9
.endmacro
