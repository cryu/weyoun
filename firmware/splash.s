; Micro-SCI Floppy Drive Controller info disassembly

PRINTVEC	:= $08
PRBL2		:= $F94A
HOME		:= $FC58
COUT		:= $FDED

.include "screencode.h"
.org $c600

        ldx     #$20	; controller signature
        ldy     #$00
        ldx     #$03
        stx     $3C

        jsr     HOME	; clear display, home cursor
        tsx
        lda     $0100,x	; current address (#$c6 for slot 6)
        sta     PRINTVEC+1
        lda     #0
        sta     PRINTVEC
        ldy     #<info	; offset of data begin
loop0:	lda     (PRINTVEC),y
	bmi     :+
        tax
        jsr     PRBL2
        beq     loop1
:	jsr     COUT
loop1:	iny
        bne     loop0
:	beq     :-

; Splash screen
;
;	<--KEYBOARD        U-SCI        REAR-->
;
;               ---------JUMPER--------
;              !           !           !
;              V           V           V
;
;             ### ! !   ! ### !   ! ! ### 
;             ### ! !   ! ### !   ! ! ### 
;             ### ! !   ! ### !   ! ! ### 
;             ### ! !   ! ### !   ! ! ### 
;             XXXXXXX   XXXXXXX   XXXXXXX
;               3.2      INFO    3.3&PASCAL
;
; Franklin differs on the first three lines:
;	<--KEYBOARD      ACE 1000      REAR-->
;
;                 ------JUMPER------

info:	screencode $0d, "<--KEYBOARD"
.ifdef MICROSCI
    screencode $08, "U-SCI", $08, "REAR-->", $0d, $0d
    screencode $08, "---------JUMPER--------", $0d
.else

    screencode $06, "ACE 1000", $06, "REAR-->", $0d, $0d
    screencode $07, " ", $02, "------JUMPER------", $0d
.endif
    screencode $07, "!", $0b, "!", $0b, "!", $0d
    screencode $07, "V", $0b, "V", $0b, "V", $0d, $0d
    screencode $06, "### ! !", $03, "! ### !", $03, "! ! ###", $0d
    screencode $06, "### ! !", $03, "! ### !", $03, "! ! ###", $0d
    screencode $06, "### ! !", $03, "! ### !", $03, "! ! ###", $0d
    screencode $06, "### ! !", $03, "! ### !", $03, "! ! ###", $0d

    screencode $06, "XXXXXXX", $03, "XXXXXXX", $03, "XXXXXXX", $0d
    screencode $08, "3.2", $06, "INFO", $04, "3.3&PASCAL", $0d, $0d
